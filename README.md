# README.md for Kaniko-Component Repository

## Overview

The kaniko-component provides a streamlined solution for building container images from a Dockerfile directly from a GitLab repository using kaniko. This method simplifies the process of image creation, bypassing the need for a full Docker environment. It's particularly useful for continuous integration/continuous deployment (CI/CD) pipelines within GitLab.

## Input Fields

The `kaniko-build-job.yml` template uses the following inputs to customize the Docker image building process:

1. `image`
   - `Description`: Name (reference) of the image to build.
   - `Default`: `${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}`

2. `context`
   - `Description`: The build context used by Kaniko.
   - `Default`: `${CI_PROJECT_DIR}`

3. `dockerfile`
   - `Description`: Path to the Dockerfile to build.
   - `Default`: `${CI_PROJECT_DIR}/Dockerfile`

3. `workload_identity_provider`
   - `Description`: workload identity provider to use, example - iam.googleapis.com/projects/PROJECT_NUMBER/locations/global/workloadIdentityPools/POOL_ID/group/GROUP_ID

## Artifacts

The primary artifact produced by this component is a Docker image, built according to the specifications in the `Dockerfile` and the input parameters provided in the `kaniko-build-job.yml`.

## Authentication and Security

The component includes a mechanism for authenticating with Google Cloud Platform (GCP), using a [federated workload identity provider](https://cloud.google.com/iam/docs/workload-identity-federation). This ensures secure access to GCP services during the build process.

## Usage
See below for an example on how to use the kaniko-component.  A full example repo showing this working is here - https://gitlab.com/aaron-prindle/test-kaniko-component


`.gitlab-ci.yml`
```
include:
  - component: gitlab.com/aaron-prindle/kaniko-component/kaniko-build-job@e5a5c8bd957f5c3416f6c28d929f0f49d28d4351
    inputs:
      image: us-central1-docker.pkg.dev/gitlab-zhangquan/gitlab/aprindle-kaniko-component-test:v1

```

## Additional Information

- [kaniko documentation](https://github.com/GoogleContainerTools/kaniko) for more details on image building.
- [GitLab CI/CD Components documentation](https://docs.gitlab.com/ee/ci/components/)
